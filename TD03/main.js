console.clear();
const verifyDriver = (age, license, acident, timeContract) => {
    const driver = {
        age: age,
        timeLicense: license,
        anyAcident: acident,
        timeInContract: timeContract
    }

    if (driver.age < 25) {
        if (driver.timeLicense < 2) {
            if (driver.anyAcident < 1) {
                console.log('Tarif: D');
            } else {
                console.log('Tarif: None');
            }
        } else {
            if (driver.anyAcident < 1) {
                console.log('Tarif: C');
            } else if (driver.anyAcident === 1) {
                console.log('Tarif: D');
            } else {
                console.log('Tarif: None');
            }
        }
    } else {
        if (driver.timeLicense > 2 && driver.anyAcident < 1 && driver.timeInContract >= 12) {
            console.log('Tarif: A');
        } else {
            if (driver.timeLicense < 2) {
                if (driver.anyAcident < 1) {
                    console.log('Tarif: C');
                } else if (driver.anyAcident === 1) {
                    console.log('Tarif: D');
                } else {
                    console.log('Tarif: None');
                }
            } else {
                if (driver.anyAcident < 1) {
                    console.log('Tarif: B');
                } else if (driver.anyAcident === 1) {
                    console.log('Tarif: C');
                } else if (driver.anyAcident === 2) {
                    console.log('Tarif: D');
                } else {
                    console.log('Tarif: None');
                }
            }
        }
    }
}

// age(Number), timeLicense(in years), anyAcident(acident numbers), timeInCompanyMonths(months)
verifyDriver(24, 3, 2, 12);


