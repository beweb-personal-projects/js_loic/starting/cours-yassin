const currentDate = {
    day: 31,
    month: 1,
    year: 2021
}

let months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

console.clear();
if (currentDate.day < 32 && currentDate.day > 0) {
    if (currentDate.month < 13 && currentDate.month > 0) {
        if (currentDate.year > 0) {

            if ((currentDate.year % 4 === 0 && currentDate.year % 100 !== 0)) {
                months[1] = 29;
            }

            let tomorrowDate = [];

            const calculateTomorrowDate = () => {
                tomorrowDate[0] = currentDate.day + 1; // Day
                tomorrowDate[1] = currentDate.month; // Month
                tomorrowDate[2] = currentDate.year; // Year

                if (tomorrowDate[0] > months[currentDate.month - 1]) tomorrowDate[0] = 1;

                if ((currentDate.day === 31) && (currentDate.month === months.length)) {
                    tomorrowDate[1] = 1;
                    tomorrowDate[2] = currentDate.year + 1;
                } else {
                    if (currentDate.day === months[currentDate.month - 1 ]) tomorrowDate[1] = currentDate.month + 1;
                    if (months[1] === 28 && currentDate.month === 2 && currentDate.day === 28) tomorrowDate[1] = currentDate.month + 1;
                    if (months[1] === 29 && currentDate.month === 2 && currentDate.day === 29) tomorrowDate[1] = currentDate.month + 1;
                }         
            }

            let yesterdayDate = [];

            const calculateYesterdayDate = () => {
                yesterdayDate[0] = currentDate.day; // Day
                yesterdayDate[1] = currentDate.month; // Month
                yesterdayDate[2] = currentDate.year; // Year

                if(currentDate.day === 1) {
                    yesterdayDate[0] = months[currentDate.month - 2];
                    yesterdayDate[1] = currentDate.month - 1;
                } else {
                    yesterdayDate[0] = currentDate.day - 1;
                }

                if ((currentDate.day === 1) && (currentDate.month === 1)) {
                    yesterdayDate[0] = 31 ; // Day
                    yesterdayDate[1] = 12; // Month
                    yesterdayDate[2] = currentDate.year - 1; // Year
                }
            }

            calculateTomorrowDate();
            calculateYesterdayDate();
            console.log(`Yesterday:  ${yesterdayDate[0]}/${yesterdayDate[1]}/${yesterdayDate[2]}`);
            console.log(`---------------------------------------------------------------------------------------------`);
            console.log(`Today:      ${currentDate.day}/${currentDate.month}/${currentDate.year}`);
            console.log(`---------------------------------------------------------------------------------------------`);
            console.log(`Tomorrow:   ${tomorrowDate[0]}/${tomorrowDate[1]}/${tomorrowDate[2]}`);

        } else {
            console.log("We cant execute the code, the year needs to be superior of 0"); 
        }
    } else {
        console.log("We cant execute the code, the month u submitted it doesnt exists"); 
    }
} else {
    console.log("We cant execute the code, the day u submitted it doesnt exist");
}